import { Container, Card, Row, Col } from "react-bootstrap";
import styles from "./CariMobil.module.css";
import { useNavigate } from "react-router-dom";
import { dataAction } from "../../Redux/action/dataAction.js";
import { useDispatch } from "react-redux";

function Cari() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  dispatch(dataAction());

  return (
    <Container>
      <Card className={styles.body}>
        <Card.Body>
          <Row>
            <Col>
              <p>Tipe Driver</p>
              <select className={styles.input}>
                <option>Pilih Tipe Driver</option>
                <option value="1">Dengan Sopir</option>
                <option value="2">Tanpa Sopir (Lepas Kunci)</option>
              </select>
            </Col>
            <Col>
              <p>Tanggal</p>
              <input className={styles.input} type="date" name="" id="" />
            </Col>
            <Col>
              <p>Waktu Jemput/Ambil</p>
              <input className={styles.input} type="time" placeholder="Pilih Waktu" />
            </Col>
            <Col>
              <p>Jumlah Penumpang</p>
              <input className={styles.input} type="number" placeholder="Jumlah Penumpang" />
            </Col>
            <Col className="m-auto">
              <button
                onClick={() => {
                  navigate("/ListCar");
                }}
                className={styles.btn}
              >
                Cari mobil
              </button>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </Container>
  );
}

export default Cari;
