import CariMobil from "../Component/CariMobil/CariMobil";
import DetailMobil from "../Component/DetailMobil/DetailMobil";

function App() {
  return (
    <div className="App">
      <>
        <CariMobil />
        <DetailMobil />
      </>
    </div>
  );
}

export default App;
