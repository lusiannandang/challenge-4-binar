import CariMobil from "../Component/CariMobil/CariMobil";
import Card from "../Component/Card/Card";

function App() {
  return (
    <div className="App">
      <>
        <CariMobil />
        <Card />
      </>
    </div>
  );
}

export default App;
