import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./Pages/Home";
import ListCar from "./Pages/ListCar";
import Detail from "./Pages/DetailCar";
import Navbar from "./Component/Navbar/Navbar";
import Footer from "./Component/Footer/Footer";

function App() {
  return (
    <div className="App">
      <>
        <BrowserRouter>
          <Navbar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/ListCar" element={<ListCar />} />
            <Route path="/DetailCar/:idCar" element={<Detail />} />
            <Route path="*" element={<h1>Page Not Found</h1>} />
          </Routes>
          <Footer />
        </BrowserRouter>
      </>
    </div>
  );
}

export default App;
